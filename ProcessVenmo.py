import csv
import sys
file_path = sys.argv[0].replace("ProcessVenmo.py","venmo_statement.csv")
print(file_path)
with open(file_path, "r") as csvin:
   csvreader = csv.reader(csvin, delimiter=',', quotechar='"')
   line = 0
   row_string = '"{}","{}","{}","{}"\n'
   with open(file_path+".out.csv","w") as csvout:
        csvout.write(row_string.format("Date","Payee","Memo","Amount"))
        for row in csvreader:
            line += 1
            if line == 3:
                coli = 0
                for col in row:
                    print(coli,col)
                    coli += 1
            if line < 5:
                continue
            tdate = row[2]

            if not tdate:
                break
            tdate = tdate[0:tdate.index("T")]
            note = row[5]
            total = row[8].replace("$","").replace(" ","").replace("+","")

            frm = row[6]
            to = row[7]
            destination = row[12]
            fsource = row[11]
            ttype = row[3]
            if ttype == "Charge":
                frm = row[7]
                to = row[6]
           
            if total.startswith("-") and not fsource == "Venmo balance":
                csvout.write(row_string.format(tdate,"Venmo","transfer: {} - Venmo".format(fsource),total.replace("-","")))
            if total.startswith("-"):
                note = "{}: {}".format(to,note)
            else:
                note = "{}: {}".format(frm, note)
            csvout.write(row_string.format(tdate,to,note,total))


